#!/usr/bin/env python3
"""
Install the requiered packages listed in requirements.txt
"""
import os
import sys


def main():
	if sys.platform in ['win32', 'cygwin']:
		prog = 'py'
	else:
		prog = 'python3'
	command = f'{prog} -m pip install -r requirements.txt'.split()
	os.execvp(command[0], command)


if __name__ == "__main__":
	main()