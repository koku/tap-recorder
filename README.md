# Tap Recorder

Teensy 3.1/3.2 code for generic rhythm reproduction task

## Specifications

The code assumes that it uses a Teensy 3.1/3.2 board with an [Audio Shield](https://www.pjrc.com/store/teensy3_audio.html)
connected with a force sensor in the pin number 3 associated with a 4.7kOhms resistor.

## Installation

To use this program, simply clone this repository and open the file `tapRecorder.ino`
inside your Arduino IDE and compile the code. The [teensyduino](https://www.pjrc.com/teensy/teensyduino.html)
extension must be installed inside your IDE for this to work!

Once the program is compiled you can upload it to your teensy device.
You can monitor the serial port to check if everything's working.

## Python monitor for the teensy

I also wrote a python code to receive taps and send commands to the teensy,
You will need the aditionnal package `pySerial` for this code to work.
You can install it automatically by executing the `setup.py` file, or you can install it using
`pip`.
Then simply run the `tester.py` script to monitor the teensy.

## Matlab API

I wrote a simple matlab API to communicate with the teensy, here's an example of basic usage (this is the content of the file `example.m`):

```matlab
% Create the recorder
recorder = TapRecorder();

% Connect to the usb device
% If the functions fails to connect, it may be that the device is
% used somewhere else, try disconnecting, rebooting matlab and try again.
% The timestamps will give seconds from this call
recorder.connect();

% Setting audio feedback
timestamp = recorder.audioFeedback(true)

% Play a file on the arduino sd card
% Warning: filenames on the arduino are limited to 8 characters (excluding
% the extention
timestamp = recorder.play("debussy.wav")

% Record a tap. 
% Adding the optionnal argument sets the timeout for this function
% (in  seconds). If no taps are detected before the timeout occurs,
% it returns an empty array
tap = recorder.recordTap(5.)

% Record a tap. Wait until the user taps indefinitely if no arguments are
% passed to the function
tap = recorder.recordTap()

% Stop all the tracks playing
timestamp = recorder.stopAll()

% Disconnect the device when finished
recorder.disconnect();
```

## Command list

Here's the list of the command that the teensy code understands:

* `play {filename}`: play the specified file (must be on the sd card)
* `stop [{track}| all]`: stop the specified track
* `feedback [on|off]`: set the audio feedback for tap (will search for a feedback.wav file at the root of sd card)
* `ls`: list all the files in the sd card with their size
* `time`: return the current time in micros since the pugin of the device.

## Tap message

If you tap on the force sensor the teensy will send a message in the following format:

`tap {onsetTime} {maxForceTime} {offsetTime} - {maxForce}`

With time in microseconds, and force between 0 and 1023.

## Limitations

* Since the int in teensy are 32 bits, the microseconds time will wrapup after 71.5 minutes of plugin.
Keep this in mind for long experiment. The matlab API take care of this wrapping up but the python
script doesn't

* The teensy tries to read the `ready` it sends when the init function is done that cause a message
on the serial port: `warning: unknown command 'ready'`. I don't really know how to fix this because I'd like to keep
the ready message...

* filenames on the SD card are crop after 12 characters (counting the extension) inside the teensy view.
It's not affecting the actual filenames on the sd card but it's affecting how you access them
from code. For example `longfilename.wav` will be accessed by `longfi~1.wav` on teensy.