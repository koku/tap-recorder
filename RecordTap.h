#ifndef _RECORD_TAP_H_
#define _RECORD_TAP_H_

struct Tap {
	unsigned long onsetTime;
	unsigned long maxForceTime;
	unsigned long offsetTime;
	int maxForce;
};

namespace tap {
	unsigned long minimumDuration = 20000; /// one tap is minimum 20 ms
	unsigned long intervalDuration = 40000; /// two taps must be separated by more than 40ms
	int upThreshold = 400; /// minimum force to apply to concider it a tap
	// 4.3kOhms thresholds = 25, 20
	int downThreshold = 300; /// maximum force for the tap to be recorded

	bool isRecording = false; /// checks to see if we are recording a tap or not
	Tap lastTap{0, 0, 0, 0}; /// stores the last recorded tap
	Tap currentTap{0, 0, 0, 0}; /// stores the current tap

	bool feedback = false;

	/**
	 * Checks if the time and the force qualify to record a tap
	 * The condition is the following:
	 *  - if we are recording, then we need to continue until the tap ends
	 *  - if we're not recording, time must be bigger than the offset time of 
	 *    the last tap plus the interval duration.
	 *    also, the force must be greater than the upThreshold.
	 */
	bool recordCondition(unsigned long time, int force){
		return tap::isRecording || (time - tap::lastTap.offsetTime >= tap::intervalDuration && force >= tap::upThreshold);
	}
	

	/**
	 * Create a new tap with the time and force
	 */
	Tap newTap(unsigned long time, int force){
		Tap res{time, time, time, force};
		return res;
	}
}

#endif // _RECORD_TAP_H_
