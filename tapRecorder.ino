#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

#include "Command.h"
#include "RecordTap.h"

// Number of the pins with the Teensy Audio Shield
#define SDCARD_CS_PIN 10
#define SDCARD_MOSI_PIN 7
#define SDCARD_SCK_PIN 14
// Number of the sensor pin to read tap
#define SENSOR_PIN 3
// Number of tracks (without feedback)
#define TRACK_NB 3


// GUItool: begin automatically generated code
// Audio tracks : Up to 3 simultaneous sounds + feedback
AudioPlaySdWav Tracks[TRACK_NB] = {AudioPlaySdWav(), AudioPlaySdWav(), AudioPlaySdWav()};
AudioPlaySdWav Feedback;

// Mixers for left and right ears
AudioMixer4 MixerLeft;
AudioMixer4 MixerRight;

// Audio output
AudioOutputI2S OuputI2S;

// Audio connections : never used variable, just for setup
AudioConnection cord1(Feedback, 0, MixerLeft, 3);
AudioConnection cord2(Feedback, 1, MixerRight, 3);
AudioConnection cord7(Tracks[0], 0, MixerLeft, 0);
AudioConnection cord8(Tracks[0], 1, MixerRight, 0);
AudioConnection cord5(Tracks[1], 0, MixerLeft, 1);
AudioConnection cord6(Tracks[1], 1, MixerRight, 1);
AudioConnection cord3(Tracks[2], 0, MixerLeft, 2);
AudioConnection cord4(Tracks[2], 1, MixerRight, 2);
AudioConnection cord9(MixerLeft, 0, OuputI2S, 0);
AudioConnection cord10(MixerRight, 0, OuputI2S, 1);
// Audio device
AudioControlSGTL5000 AudioDevice;
// GUItool: end automatically generated code

/**
 * FUNCTIONS PROTOTYPES FOR DEFAULT VALUES
 */
void lsCommand(File dir, int numTabs=0);
void error(String message, bool fatal=false);


/**
 * Initialization function
 */
void setup() {
	Serial.begin(115200);
	Serial.setTimeout(0);
	// Serial.println("init");

	// Setup of the audiomemory cache
	AudioMemory(10);
	// Activation of the audio device
	AudioDevice.enable();
	AudioDevice.volume(0.3); // default volume output
	for (int i=0; i < 4; i++){
		MixerLeft.gain(i, 0.5);
		MixerRight.gain(i, 0.5);
	}

	// Setup of the sdcard dialogue
	SPI.setMOSI(SDCARD_MOSI_PIN);
	SPI.setSCK(SDCARD_SCK_PIN);
	if (!SD.begin(SDCARD_CS_PIN)){
		error("can't connect to SD card", true);
	}
	
	// Serial.println("ready");
}


/**
 * Loop function: called every millisecond by hardware
 * IDLE STATE: takes care of updating all the inputs
 * 	- updates time
 * 	- reads the force on the sensor
 * 	- reads the usb serial to listen to commands from the computer
 * 	- checks if a file playing stopped (need of a variable)
 */
void loop() {
	unsigned long time = micros(); // update of the time
	unsigned int force = analogRead(SENSOR_PIN); // read the tap sensor
	//Serial.println(String(force));

	// Read for computer command
	//
	//*
	Command cmd = readCommand();
	//Serial.println("Read stop: " + String(micros()));

	// checks the conditions to go to an other state
	if (tap::recordCondition(time, force)){
		record(time, force);
	}

	// check the computer command
	switch (cmd.type){
		case START:
			startCommand(time, cmd.arg);
			break;
		case STOP:
			stopCommand(time, cmd.arg);
			break;
		case FEEDBACK:
			feedbackCommand(time, cmd.arg);
			break;
		case TIME:
			Serial.println("time - " + String(time));
			break;
		case LIST:
			lsCommand(SD.open("/"));
			break;
		default:
			// Do nothing
			;
	}
	// */
}


/**
 * Record a tap
 */
void record(unsigned long time, int force){
	using namespace tap;
	if (!isRecording) {
		// if we were not recording any tap, it means we're in a new tap
		isRecording = true;
		currentTap = newTap(time, force);
		if (feedback) {
			Feedback.play("feedback.wav");
		}
	} else {
		// update the offsetTime
		currentTap.offsetTime = time;
		// checks if the tap has ended
		if (force < downThreshold){
			if (time - currentTap.onsetTime >= minimumDuration){
				// this is a real Tap
				tapSignal(currentTap);
				lastTap = currentTap;
			}
			isRecording = false;
		}
		
		// checks the max force
		if (currentTap.maxForce < force){
			currentTap.maxForceTime = time;
			currentTap.maxForce = force;
		}
	}
}


/**
 * Send tap to the user
 * format : tap onsetTime maxForceTime offsetTime - maxForce
 */
void tapSignal(Tap tap){
	String msg = "tap ";
	msg += String(tap.onsetTime) + " ";
	msg += String(tap.maxForceTime) + " ";
	msg += String(tap.offsetTime) + " - ";
	msg += String(tap.maxForce);
	Serial.println(msg);
}



/**
 * Start playing the file `filename`
 * 
 */
void startCommand(unsigned long time, String filename){
	const char *file = filename.c_str();
	if (!filename.endsWith(".wav")){
		error("`" + filename + "` is not a wav file");
		return;
	}
	if (!SD.exists(file)){
		error("`" + filename + "` doesn't exist");
		return;
	}

	bool noTracksAvailable = false;
	for (int i = 0; i < TRACK_NB; i++){
		noTracksAvailable = false;
		if (!Tracks[i].isPlaying()){
			Tracks[i].play(file);
			startSignal(time, filename, i);
			break;
		} else {
			noTracksAvailable = true;
		}
	}

	if (noTracksAvailable) {
		error("no tracks available");
	}
}


/**
 * Send a message that notify the user that filename has started playing on the
 * track tracknumber
 */
void startSignal(unsigned long time, String filename, int trackNumber){
	Serial.println(String(time) + " - track " + String(trackNumber + 1) + " - started " + filename); 
}


/**
 * 
 */
void stopCommand(unsigned long time, String trackNumber){
	if (trackNumber == "all"){
		for (int i=0; i < TRACK_NB; i++){
			Tracks[i].stop();
			stopSignal(time, i);
		}
		return;
	}
	
	int number = trackNumber.toInt() - 1;
	
	if (number < 0 || number >= TRACK_NB){
		error("track number out of range");
		return;
	}
	
	Tracks[number].stop();
	stopSignal(time, number);
}


/**
 * 
 */
void stopSignal(unsigned long time, int trackNumber){
	Serial.println(String(time) + " - track " + String(trackNumber + 1) + " - stopped");
}


/**
 * 
 */
void feedbackCommand(unsigned long time, String state){
	if (state == "on"){
		tap::feedback = true;
	} else if (state == "off"){
		tap::feedback = false;
	} else {
		error("unknown feedback state `" + state + "`");
		return;
	}
	feedbackSignal(time, state);
}


/**
 * 
 */
void feedbackSignal(unsigned long time, String state){
	Serial.println(String(time) + " - feedback " + state);
}


/**
 * Read the command sent by the computer
 */
Command readCommand(){
	Command cmd{LOOP, ""};
	if (Serial.available() > 0){
		String input = Serial.readString();
		input.trim(); // removing ending newline

		if (input.startsWith("play")){
			// State START
			cmd.type = START;
			cmd.arg = input.substring(5);
		} else if (input.startsWith("stop")) {
			// State STOP
			cmd.type = STOP;
			cmd.arg = input.substring(5);
		} else if (input.startsWith("feedback")) {
			// State FEEDBACK
			cmd.type = FEEDBACK;
			cmd.arg = input.substring(9);
		} else if (input == "ls") {
			cmd.type = LIST;
		} else if (input == "time") {
			cmd.type = TIME;
		} else {
			int index = input.length();
			for (int i=1; i < input.length(); i++){
				if (isWhitespace(input.charAt(i))) {
					index = i;
					break;
				}
			}
			error("unknown command `" + input.substring(0, index) + "`");
		}
	}
	return cmd;
}


void lsCommand(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      lsCommand(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}


/**
 * Send an error message to the computer and stop the execution if 
 * the error is fatal
 * send error if the execution stops
 * send warning if the execution continues
 */
void error(String message, bool fatal){
	String prefix = (fatal) ? "error: " : "warning: ";
	String errmsg = prefix + message;
	Serial.println(errmsg);
	// Trap loop
	while (fatal) {
		delay(2000);
		Serial.println(errmsg);
	}
}
