% This script is a minimal example of use of the API

% Create the recorder
recorder = TapRecorder();

% Connect to the usb device
% If the functions fails to connect, it may be that the device is
% used somewhere else, try disconnecting, rebooting matlab and try again.
% The timestamps will give seconds from this call
recorder.connect();

% Setting audio feedback
timestamp = recorder.audioFeedback(true)

% Play a file on the arduino sd card
% Warning: filenames on the arduino are limited to 8 characters (excluding
% the extention
timestamp = recorder.play("debussy.wav")

% Record a tap. 
% Adding the optionnal argument sets the timeout for this function
% (in  seconds). If no taps are detected before the timeout occurs,
% it returns an empty array
tap = recorder.recordTap(5.)

% Record a tap. Wait until the user taps indefinitely if no arguments are
% passed to the function
tap = recorder.recordTap()

% Stop all the tracks playing
timestamp = recorder.stopAll()

% Disconnect the device when finished
recorder.disconnect();
