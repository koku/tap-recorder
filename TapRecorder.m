classdef TapRecorder < handle
    %TAPRECORDER Arduino interface
    %   This object make the interface between the arduino
    %   And your matlab code

    properties (Access = private)
        lastTime = 0    % stored in microseconds
        cycleDuration = 2 ^ 32
        nbCycle = 0
    end
    
    properties (Access = public)
        isConnected = false
        startupTime = 0 % stored in microseconds
        device
        tapBuffer
    end
    
    methods (Access = public)
        function obj = TapRecorder(port)
            %TAPRECORDER Construct an instance of this class
            %   Detailed explanation goes here
            if nargin == 0
                if ispc()
                    error("Windows support not provided yet");
                elseif ismac()
                    port = '/dev/tty.usbmodem4487041';
                elseif isunix()
                    port = '/dev/ttyACM0';
                end
            end
            disp(['Using port `' port '`'])
            obj.device = serial(port, 'BaudRate', 115200);
        end
        
        function connect(obj)
            %CONNECT Connect to the arduino
            fopen(obj.device);
            obj.isConnected = true;
            % flush the buffer
            flushinput(obj.device);
            % get the time
            fprintf(obj.device, "time");
            obj.startupTime = obj.expect(RepType.TIME);
        end
        
        function disconnect(obj)
            %DISCONNECT Disconnect from the arduino
            obj.validateconnection();
            obj.audioFeedback(false);
            obj.stopAll();
            fclose(obj.device);
            obj.isConnected = false;
        end
        
        function timestamp = audioFeedback(obj, state)
            %AUDIOFEEDBACK Enable or Disable audio feedback
            %   Returns the timestamp in arduino ref of the event
            validateattributes(state, {'logical'}, {'scalar'});
            obj.validateconnection();
            if state
                statemsg = "on";
            else
                statemsg = "off";
            end
            msg = ["feedback"; statemsg];
            fprintf(obj.device, strjoin(msg));
            % Wait the response before resuming the program
            reply = obj.expect(RepType.FEEDBACK);
            timestamp = reply(1);
        end
        
        function [timestamp, track] = play(obj, filename)
            validateattributes(filename, {'string'}, {'scalar'});
            obj.validateconnection();
            msg = ["play"; filename];
            fprintf(obj.device, strjoin(msg));
            reply = obj.expect(RepType.PLAY);
            timestamp = reply(1);
            track = reply(2);
        end
        
        function timestamp = stop(obj, track)
            validateattributes(track, {'numeric'}, {'scalar', '>=', 1, '<=', 3});
            obj.validateconnection();
            msg = ["stop"; string(track)];
            fprintf(obj.device, strjoin(msg));
            reply = obj.expect(RepType.STOP);
            timestamp = reply(1);
        end
        
        function timestamp = stopAll(obj)
            obj.validateconnection();
            msg = "stop all";
            fprintf(obj.device, msg);
            reply = obj.expect(RepType.STOP);
            obj.expect(RepType.STOP);
            obj.expect(RepType.STOP);
            timestamp = reply(1);
        end
        
        function tap = recordTap(obj, timeout)
            %RECORD read and return the next tap made by the user
            %   param timeout: returns a empty tap after timeout second
            obj.validateconnection();
            if nargin == 1
                reply = obj.expect(RepType.TAP);
            elseif nargin == 2
                obj.device.ReadAsyncMode = 'manual';
                obj.device.Timeout = timeout;
                readasync(obj.device);
                try
                    reply = split(strtrim(fgetl(obj.device)));
                    reply = [obj.time_s(str2double(reply([2 3 4]))); str2double(reply(6))];
                catch
                    reply = [];
                end
                stopasync(obj.device);
                obj.device.ReadAsyncMode = 'continuous';
            end 
            tap = reply;
        end
    end
    
    methods (Access = private)
        function validateconnection(obj)
            %VALIDATECONNECTION assert that the serial port is open
            assert(...
                obj.isConnected,...
                "Connection to arduino not established, use `connect()` to establish connection"...
            )
        end
        
        function [type, data] = parseline(obj)
            %PARSELINE wait and return the first line send by arduino
            %   splited by space and without trailing newline
            ok=false;
            while ~ok
                if obj.device.BytesAvailable
                    reply = split(strtrim(fgetl(obj.device)));
                    ok = true;
                end
            end
            % disp(strjoin(reply))
            if reply(1) == "error:"
                type = RepType.ERROR;
                data = [];
                error(strjoin(reply));
            elseif reply(1) == "tap"
                type = RepType.TAP;
                data = [obj.time_s(str2double(reply([2 3 4]))); str2double(reply(6))];
            elseif reply(1) == "time"
                type = RepType.TIME;
                data = obj.time_s(str2double(reply(3)));
            else
                timestamp = obj.time_s(str2double(reply(1)));
                assert(~isnan(timestamp));
                if reply(3) == "feedback"
                    type = RepType.FEEDBACK;
                    if reply(4) == "on"
                        state = 1;
                    else
                        state = 0;
                    end
                    data = [timestamp state];
                elseif reply(6) == "started"
                    type = RepType.PLAY;
                    track = str2double(reply(4));
                    data = [timestamp track];
                elseif reply(6) == "stopped"
                    type = RepType.STOP;
                    track = str2double(reply(4));
                    data = [timestamp track];
                end
                
            end
        end
        
        function reply = expect(obj, expected)
            %EXPECT wait until the expected state occurs
            [repType, data] = obj.parseline();
            while repType ~= expected
                [repType, data] = obj.parseline();
            end
            reply = data;
        end
        
        function timestamp = time_s(obj, time)
            %TIME_S return the time in seconds
            %   param time: time sent by arduino (microseconds)
            %   takes care of cycling problem
            timestamp = zeros(size(time));
            for i=1:length(time)
                if time(i) < obj.lastTime
                    obj.nbCycle = obj.nbCycle + 1;
                end
                obj.lastTime = time(i);
                timestamp(i) = (obj.nbCycle * obj.cycleDuration...
                                + time(i)) / 1000000 - obj.startupTime;
            end
        end
    end
end
