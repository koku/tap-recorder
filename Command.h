#ifndef _TAP_RECORDER_COMMAND_H_
#define _TAP_RECORDER_COMMAND_H_

enum State {
	LOOP,
	START,
	STOP,
	FEEDBACK,
	TIME,
	LIST,
};

struct Command {
	State type;
	String arg;
};

#endif // _TAP_RECORDER_COMMAND_H_
