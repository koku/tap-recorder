#!/usr/bin/env python3
import sys
from serial import Serial


def main():
	dev = Serial("/dev/ttyACM0", baudrate=10000000)
	for line in sys.stdin:
		dev.write(bytes(line, "utf-8"))
		print(dev.readline().decode("utf-8"), end="")
	dev.close()


if __name__ == "__main__":
	main()

