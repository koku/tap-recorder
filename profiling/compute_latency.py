#!/usr/bin/env python3
import numpy as np


FILES = [f"latency{i}.txt" for i in range(10)]


def main():
	delays = np.zeros((len(FILES), 10000))
	for i, filename in enumerate(FILES):
		times = np.loadtxt(filename, dtype=np.int)
		delays[i, :] = np.diff(times)
	print("DELAYS CALCULATED ON 100 000 PING REQUEST (results in μs)")
	print(f"min:{np.min(delays)}\tmean:{np.mean(delays)}\tstd:{np.std(delays)}\tmax:{np.max(delays)}")
	print(f"95% interval:{np.quantile(delays, [0.025, 0.975])}")


if __name__ == "__main__":
	main()
