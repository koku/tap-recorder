#!/usr/bin/env python3
"""
Tester app for the TeensyTap device
"""
import sys
import serial
from threading import Thread


def main():
	dev = serial.Serial('/dev/ttyACM0', timeout=0.0001, baudrate=10000000)

	class Stdin(Thread):
		def __init__(self):
			super().__init__()

		def run(self) -> None:
			for line in sys.stdin:
				if line == '\n':
					return
				dev.write(bytes(line, 'utf-8'))

	stdin = Stdin()
	stdin.start()
	arduino = iter(dev)
	while stdin.is_alive():
		try:
			msg = next(arduino)
			print(msg.decode('utf-8'), end='')
		except StopIteration:
			pass

	dev.close()


if __name__ == "__main__":
	main()
